%% Script to generate Figure 1 illustrated in the paper

% Test case: Throttle input signal
% Step signal with three parameters: Step time, Initial Value, Final Value

%% Test 1 : both the original model and the mutant satisfy the property
ST_test1 = 5.0471;
IV_test1 = 55.0310;
FV_test1 = 51.3736;

%% Test 2 : the original model satisfies the property while the mutant violates the property
ST_test2 = 1;
IV_test2 = 32;
FV_test2 = 66;

%% Simulate original model for both tests

%% Load the Original model
model = 'ATCS_fault_injector/cav_benchmark/AutotransModel.slx';
[ model_path, model_name_wo_ext, model_ext ] = fileparts( model );
model_path = [ model_path, '/' ];
system = load_system([model_path, model_name_wo_ext, model_ext]);
input_name = strcat(model_name_wo_ext,'/Throttle');
save_system(system);

%% Simulate the original model for Test 1

% Set up the input (throttle) parameters: Step time, Initial value, Final value
set_param(input_name, 'Time', num2str(ST_test1), 'Before', num2str(IV_test1), 'After', num2str(FV_test1));
simOut = sim(model_name_wo_ext, 'SaveTime', 'on', 'TimeSaveName', 'tout',...
    'SaveState', 'on', 'StateSaveName', 'xoutNew', 'SaveOutput', 'on', ...
    'OutputSaveName', 'youtNew', 'SignalLogging', 'on', 'SignalLoggingName', 'logsout');
data = simOut.get('logsout');

% Output signals corresponding to the input signal
Enginespeed_o1 = data{2}.Values.Data; % s55 % Data points engine speed
Vehiclespeed_o1 = data{33}.Values.Data; % s42 % Data points vehicle speed

%% Simulate the original model for Test 2

% Set up the input (throttle) parameters: Step time, Initial value, Final value
set_param(input_name, 'Time', num2str(ST_test2), 'Before', num2str(IV_test2), 'After', num2str(FV_test2));
simOut = sim(model_name_wo_ext, 'SaveTime', 'on', 'TimeSaveName', 'tout',...
    'SaveState', 'on', 'StateSaveName', 'xoutNew', 'SaveOutput', 'on', ...
    'OutputSaveName', 'youtNew', 'SignalLogging', 'on', 'SignalLoggingName', 'logsout');
data = simOut.get('logsout');

% Output signals corresponding to the input signal
Enginespeed_o2 = data{2}.Values.Data; % s55 % Data points engine speed
Vehiclespeed_o2 = data{33}.Values.Data; % s42 % Data points vehicle speed

%% Save and Close original model
save_system(system);
close_system(system);

%% Initialize Fault Injection Library of FIM
addpath('FaultInjector_Master');
load_system('FInjLib');
set_param('FInjLib', 'Lock', 'off');

%% Load the mutant
model = 'ATCS_fault_injector/cav_benchmark/AutotransModel_14.slx';
[ model_path, model_name_wo_ext, model_ext ] = fileparts( model );
model_path = [ model_path, '/' ];
system = load_system([model_path, model_name_wo_ext, model_ext]);
input_name = strcat(model_name_wo_ext,'/Throttle');
save_system(system);

%% Simulate the mutant for Test 1

% Set up the input (throttle) parameters: Step time, Initial value, Final value
set_param(input_name, 'Time', num2str(ST_test1), 'Before', num2str(IV_test1), 'After', num2str(FV_test1));
simOut = sim(model_name_wo_ext, 'SaveTime', 'on', 'TimeSaveName', 'tout',...
    'SaveState', 'on', 'StateSaveName', 'xoutNew', 'SaveOutput', 'on', ...
    'OutputSaveName', 'youtNew', 'SignalLogging', 'on', 'SignalLoggingName', 'logsout');
data = simOut.get('logsout');

% Output signals corresponding to the input signal
Enginespeed_mut1 = data{2}.Values.Data; % s55 % Data points engine speed
Vehiclespeed_mut1 = data{33}.Values.Data; % s42 % Data points vehicle speed

%% Simulate the mutant for Test 2

% Set up the input (throttle) parameters: Step time, Initial value, Final value
set_param(input_name, 'Time', num2str(ST_test2), 'Before', num2str(IV_test2), 'After', num2str(FV_test2));
simOut = sim(model_name_wo_ext, 'SaveTime', 'on', 'TimeSaveName', 'tout',...
    'SaveState', 'on', 'StateSaveName', 'xoutNew', 'SaveOutput', 'on', ...
    'OutputSaveName', 'youtNew', 'SignalLogging', 'on', 'SignalLoggingName', 'logsout');
data = simOut.get('logsout');

% Output signals corresponding to the input signal
Enginespeed_mut2 = data{2}.Values.Data; % s55 % Data points engine speed
Vehiclespeed_mut2 = data{33}.Values.Data; % s42 % Data points vehicle speed

%% Save and Close the mutant model
save_system(system);
close_system(system);

%% Plot the results

figure;
Time_u = data{2}.Values.Time; % Time points

% Plot vehicle speed for original model and mutant for Test 1
subplot(2,2,1);
plot(Time_u, Vehiclespeed_o1, 'LineWidth', 2);           
hold on;
plot(Time_u, Vehiclespeed_mut1, 'LineWidth', 2);           
hold on;
Threshold = 120*ones(1,751); % Vehicle speed threshold set to 120 mph
plot(Time_u, Threshold, 'LineWidth', 2);    
xlabel('Time (seconds)');
ylabel('Vehicle Speed (mph)');
xlim([0 30]);
ylim([0 130]);
legend('Original model', 'Mutant','Threshold');
title('Output (Vehicle Speed)');

% Plot engine speed for original model and mutant for Test 1
subplot(2,2,2);
plot(Time_u, Enginespeed_o1, 'LineWidth', 2);           
hold on;
plot(Time_u, Enginespeed_mut1, 'LineWidth', 2);         
hold on
Threshold_rpm = 4500*ones(1,751); % Engine speed threshold set to 4500 RPM
plot(Time_u, Threshold_rpm, 'LineWidth', 2);           
xlabel('Time (seconds)');
ylabel('Engine Speed (RPM)');
xlim([0 30]);
ylim([0 4600]);
legend('Original model', 'Mutant','Threshold');
title('Output (Engine Speed)');

% Plot vehicle speed for original model and mutant for Test 2
subplot(2,2,3);
plot(Time_u, Vehiclespeed_o2, 'LineWidth', 2);           
hold on;
plot(Time_u, Vehiclespeed_mut2, 'LineWidth', 2);           
hold on;
Threshold = 120*ones(1,751); % Vehicle speed threshold set to 120 mph
plot(Time_u, Threshold, 'LineWidth', 2);    
xlabel('Time (seconds)');
ylabel('Vehicle Speed (mph)');
xlim([0 30]);
ylim([0 130]);
legend('Original model', 'Mutant','Threshold');
title('Output (Vehicle Speed)');

% Plot engine speed for original model and mutant for Test 2
subplot(2,2,4);
plot(Time_u, Enginespeed_o2, 'LineWidth', 2);           
hold on;
plot(Time_u, Enginespeed_mut2, 'LineWidth', 2);         
hold on
Threshold_rpm = 4500*ones(1,751); % Engine speed threshold set to 4500 RPM
plot(Time_u, Threshold_rpm, 'LineWidth', 2);           
xlabel('Time (seconds)');
ylabel('Engine Speed (RPM)');
xlim([0 30]);
ylim([0 4600]);
legend('Original model', 'Mutant','Threshold');
title('Output (Engine Speed)');



