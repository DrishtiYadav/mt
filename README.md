# MT

This repository contains the data and code for the experiments.

# Installation
Our experiments require a valid license of MATLAB from MathWorks. To run the scripts, it is recommended to download and install the full package of MATLAB (R2018b) with Simulink.

You also need to install the following:

(1) [RTAMT](https://github.com/nickovic/rtamt)
(2) [FIM](https://gitlab.com/DrishtiYadav/fimtool)

# Details
In our open-source implementation, we provide two usage examples:
(1) Automatic Transmission Controller System (ATCS) 
(2) Aircraft Elevator Control System (AECS)

The folder ‘ATCS_fault_injector’ consists of the ATCS Simulink model and all the mutants generated for the ATCS model. The folder ‘AECS_fault_injector’ consists of the AECS Simulink model and all its mutants.

The folders 'ResultsATCS' and 'ResultsAECS' consist of fault tables providing a detailed description of all the generated mutants for ATCS and AECS, respectively. 

# Requisites

Copy the folder ‘FaultInjector_Master’ within the [FIM](https://gitlab.com/DrishtiYadav/fimtool) package to the main directory of MT. 

Since [FIM](https://gitlab.com/DrishtiYadav/fimtool) is created in R2020b and we perform the experiments in R2018b, you first need to do the following to ensure compatibility: 

(1) Open MATLAB R2020b.
(2) Navigate through the folder ‘FaultInjector_Master’.
(3) Open 'FInjLib.slx'.
(4) In the Simulink Editor, on the Simulation tab, select Save > Previous Version. In the Export to Previous Version dialog box, from the Save as type list, select the version R2018b to which we export the model.
(5) Click Save.
(6) Close MATLAB R2020b.

Now open MATLAB R2018b to conduct the experiments.

# Usage 

Description of the mutant that we considered in Figure 1: 'AutotransModel_14.slx' in which a 'Bias/Offset' fault is injected in the 'Transmission/TransmissionRatio' component of the ATCS model.

A test case is a signal representing the throttle input. In our example, we assume that the throttle is a step signal characterized by three parameters: (step time, initial value, final value)
- Test 1 (Both Original model and mutant satisfy the property): (5.0471, 55.0310, 51.3736)
- Test 2 (Original model satisfies the property and the mutant violates the property): (1, 32, 66)  

To generate the plot (as shown in Figure 1 in the paper), run the script 'plotfig1.m'.

The number of mutants reported in Table II of the paper can be verified from the fault tables stored in the folders 'ResultsATCS' and 'ResultsAECS'.
 
## Mutation analysis
- Tests with ATCS and its mutants: run the script 'ATCS_MA.m'
- Tests with AECS and its mutants: run the script 'AECS_MA.m'

All the results will be stored (as variables) in the MATLAB Workspace. 
