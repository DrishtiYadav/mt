%% ATCS Mutation Analysis
% Script to simulate the original model and the generated mutants, compute
% actual mutation score and MS

%% Simulation of the models for different test cases in different test suites
% Running the model multiple times in parallel using 'parsim' command

warning('off','all');

%% Initialization

addpath('FaultInjector_Master');
load_system('FInjLib');
set_param('FInjLib', 'Lock', 'off');

%% Add folders to path and load the initial test suite

addpath('ATCS_fault_injector');
addpath('AECS_fault_injector');
addpath('Test Generation')
addpath('ResultsATCS');
load('Testcase_initATCS_ART.mat');
addpath('Configuration');
load('ModelConstantsThresholds_autotransmod04.mat');

%% Test suite

TSuite = [Testcase_initATCS_ART]; 

for gg = 1%1:length(TSuite) % for all the test suites, do the following
    
    Testcase_init = TSuite(gg,:);
    
    tic; % to observe execution time of original model and the mutants
    
    %% Original model
    model = 'ATCS_fault_injector/cav_benchmark/AutotransModel.slx';
    [ model_path, model_name_wo_ext, model_ext ] = fileparts( model );
    model_path = [ model_path, '/' ];
    
    % Load the system
    system = load_system([model_path, model_name_wo_ext, model_ext]);
    input_name = strcat(model_name_wo_ext,'/Throttle');
    save_system(system);
    
    % Create an array of SimulationInput objects.
    for i = 1:length(Testcase_init)
        in(i) = Simulink.SimulationInput(model_name_wo_ext);
        in(i) = in(i).setBlockParameter(input_name,'Time', num2str(Testcase_init{1,i}(1)));
        in(i) = in(i).setBlockParameter(input_name,'Before', num2str(Testcase_init{1,i}(2)));
        in(i) = in(i).setBlockParameter(input_name,'After', num2str(Testcase_init{1,i}(3)));
    end
    
    out_Original = parsim(in, 'ShowProgress', 'off');
    SOut_orig{gg} = out_Original; % Store Simulation output of original model for test suite number 'gg'
    
    %% Simulate the mutants (i.e., the faulty versions)
    
    %% Gather mutant information
    newtable = readtable(strcat('ResultsATCS', '/Fault_table.xlsx'));
    
    %% Mutant execution
%     fprintf('\n');
%     disp('Starting simulation of all mutants on all test cases....');
%     fprintf('\n');
    for pq = 1 : height(newtable) % for all mutants in the fault table, do the following
        modelname = cell2mat(table2array(newtable(pq,1)));
        
        % Load the system
        system = load_system([model_path, modelname, model_ext]);
        set_param(system, 'AutoInsertRateTranBlk','on');
        set_param(system, 'Solver', 'ode4', 'StopTime', '30', 'ReturnWorkspaceOutputs', 'on');
%         disp(strcat('Simulating the mutant :  ', modelname));
        input_name = strcat(modelname,'/Throttle');
        
        % Create an array of SimulationInput objects.
        for i = 1:length(Testcase_init)
            in(i) = Simulink.SimulationInput(modelname);
            in(i) = in(i).setBlockParameter(input_name,'Time', num2str(Testcase_init{1,i}(1)));
            in(i) = in(i).setBlockParameter(input_name,'Before', num2str(Testcase_init{1,i}(2)));
            in(i) = in(i).setBlockParameter(input_name,'After', num2str(Testcase_init{1,i}(3)));
        end
        
        save_system(system);
        out_mutant{pq} = parsim(in, 'ShowProgress', 'off');
%         fprintf('\n');
    end
    SOut_mut{gg} = out_mutant; % Store Simulation output of mutants for test suite number 'gg'
    execution_time{gg} = toc;  % Keep track of execution time for each test suite
    
    %% Evaluate simulation data to compute mutation scores
    
    for nb_test = 1:length(out_Original)
        data = out_Original(1,nb_test).get('logsout');
        data_original{nb_test} = data;
        signalNames_o{nb_test} = getElementNames(data);
    end
    
    for pq = 1:height(newtable)
        for nb_test = 1:length(out_mutant{1,pq})
            data = out_mutant{1,pq}(1,nb_test).get('sldemo_autotrans_output');
            data_mutant{pq}{nb_test} = data;
            signalNames_mut{pq}{nb_test} = getElementNames(data);
        end
    end
    
    datamut = data_mutant;
    signalNames_mutant = signalNames_mut;
    data1 = data_original;
    
    % We need to check only the output signals in case of regular mutation analysis and for property-based mutation analysis, the
    % signals included in phi; in total 2 signals: Vehicle speed and Engine speed
    % find simout signals and make data array using the signals
    for j = 1: length(data1)
        % fetch signal s42
        signal1 = find(data1{1, j},'Name','s42');
        % fetch signal s55
        signal2 = find(data1{1, j},'Name','s55');
        % make a new dataset using these two signals
        simdata_o{1,j} = concat(signal1,signal2);
    end
    
    for l = 1 : height(newtable) % mutant number
        for  j = 1:1:length(out_mutant{1,l})
            % fetch signal s236
            signal1 = find(datamut{1,l}{1, j},'Name','s42');
            % fetch signal s237
            signal2 = find(datamut{1,l}{1, j},'Name','s55');
            % make a new dataset using these two signals
            simdata_mut{1,l}{1,j} = concat(signal1,signal2);
        end
    end
    
    %% ----------------------------------------------------------------------------------
    %% Code for fetching and analyzing simulation data for PMA (property-based mutation analysis) and CMA (classical or regular mutation analysis)
    
    %% Original model: Compute robustness values for output signals for all test cases
    % Computing robustness of the original model for every test case
    
    for nb_test = 1:length(out_Original)
        data = simdata_o{nb_test};
        signalNames = signalNames_o{nb_test};
        start_time = 0;
        stop_time = 30;
        dt = 0.04;
        index = 1;
        signal_length = 751;
        for k = 1:length(getElementNames(data))
            logsoutElem(k) = get(data,k);
            zero_add_above = 0;
            zero_add_below = 0;
            if (isprop(logsoutElem(k).Values,'Data'))
                logsoutData_woi = logsoutElem(k).Values.Data;
                [m_logsoutData_woi,n_logsoutData_woi] = size(logsoutData_woi);
                if (n_logsoutData_woi == 1)
                    if (m_logsoutData_woi < signal_length)
                        if m_logsoutData_woi == 0
                            timevals = start_time:dt:stop_time;
                            for i = 1:length(timevals)
                                datavals = [];
                                datavals(i) = 0;
                            end
                            logsoutData_woi = cat(1,datavals',logsoutData_woi);
                        end
                        if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(1) ~= start_time) % Append zeros to the signal in the beginning or end or both as necessary
                            zero_add_above = uint64(logsoutElem(k).Values.Time(1)/dt);
                            logsoutData_fi = [];
                            logsoutData_fi(zero_add_above,1) = 0;
                            logsoutData_woi = cat(1,logsoutData_fi,logsoutData_woi);
                        end
                        if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(end) ~= stop_time)
                            zero_add_below = signal_length - m_logsoutData_woi - zero_add_above;
                            logsoutData_fi = [];
                            logsoutData_fi(zero_add_below,1) = 0;
                            logsoutData_woi = cat(1,logsoutData_woi,logsoutData_fi);
                        end
                        if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(1) == start_time) && (logsoutElem(k).Values.Time(2) ~= (start_time+dt))
                            val = logsoutElem(k).Values.Data(1);
                            timevals = start_time:dt:(logsoutElem(k).Values.Time(2)-dt);
                            for i = 1:length(timevals)
                                datavals = [];
                                datavals(i) = val;
                            end
                            logsoutData_woi = cat(1,datavals',logsoutData_woi(2:end));
                        end
                        if (m_logsoutData_woi == 1) && (logsoutElem(k).Values.Time(1) == start_time)
                            timevals = start_time+dt:dt:stop_time;
                            for i = 1:length(timevals)
                                datavals = [];
                                datavals(i) = 0;
                            end
                            logsoutData_woi = cat(1,datavals',logsoutData_woi);
                        end
                        logsoutData_array(:,index) = double(logsoutData_woi);
                    else
                        logsoutData_array(:,index) = double(logsoutData_woi);
                    end
                    logsoutData_final{index} = logsoutData_woi;
                    index = index + 1;
                end
            else
                logsoutData_struct = logsoutElem(k).Values;
                logsoutData_cell = struct2cell(logsoutData_struct);
                [m_logsoutData_cell,n_logsoutData_cell] = size(logsoutData_cell);
                for l = 1:m_logsoutData_cell
                    logsoutData_w = logsoutData_cell{l}.Data;
                    logsoutData_wi = logsoutData_cell{l}.Data;
                    [m_logsoutData_wi,n_logsoutData_wi] = size(logsoutData_wi);
                    if (m_logsoutData_wi < signal_length)
                        if (logsoutData_cell{1}.Time(1) ~= start_time) % Append zeros to the signal in the beginning or end or both as necessary
                            zero_add_above = uint64(logsoutData_cell{1}.Time(1)/dt);
                            logsoutData_f = [];
                            logsoutData_f(zero_add_above,n_logsoutData_wi) = 0;
                            logsoutData_w = cat(1,logsoutData_f,logsoutData_wi);
                        end
                        if (logsoutData_cell{1}.Time(end) ~= stop_time)
                            zero_add_below = signal_length - uint64(logsoutData_cell{1}.Time(end)/dt) - zero_add_above;
                            logsoutData_f = [];
                            logsoutData_f(zero_add_below,n_logsoutData_wi) = 0;
                            logsoutData_w = cat(1,logsoutData_wi,logsoutData_f);
                        end
                    end
                    if (n_logsoutData_wi == 1)
                        logsoutData_array(:,index) = double(logsoutData_w);
                        logsoutData_final{index} = logsoutData_w;
                        index = index + 1;
                    end
                end
            end
        end
        simout = logsoutData_array;
        SIMOUT_Original{nb_test} = simout;
        rob_o{nb_test} = monitorATCS(simout);
    end
    
    ROB_ORIG{gg} = rob_o;
    
    %% Mutants (Faulty models): Compute robustness values for output signals for all test cases
    % Computing robustness of all mutants for every test case
    for pq = 1:height(newtable) % for each mutant
        for nb_test = 1:length(out_mutant{1,pq}) % for each test case
            data = simdata_mut{pq}{nb_test};
            signalNames = signalNames_mutant{pq}{nb_test};
            start_time = 0;
            stop_time = 30;
            dt = 0.04;
            index = 1;
            signal_length = 751;
            for k = 1:length(getElementNames(data))
                logsoutElem(k) = get(data,k);
                zero_add_above = 0;
                zero_add_below = 0;
                logsoutName = logsoutElem(k).Values.Name; % Not all Values may have Names for Data
                if (isprop(logsoutElem(k).Values,'Data'))
                    logsoutData_woi = logsoutElem(k).Values.Data;
                    [m_logsoutData_woi,n_logsoutData_woi] = size(logsoutData_woi);
                    if (n_logsoutData_woi == 1)
                        if (m_logsoutData_woi < signal_length)
                            if m_logsoutData_woi == 0
                                timevals = start_time:dt:stop_time;
                                for i = 1:length(timevals)
                                    datavals = [];
                                    datavals(i) = 0;
                                end
                                logsoutData_woi = cat(1,datavals',logsoutData_woi);
                            end
                            if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(1) ~= start_time) % Append zeros to the signal in the beginning or end or both as necessary
                                zero_add_above = uint64(logsoutElem(k).Values.Time(1)/dt);
                                logsoutData_fi = [];
                                logsoutData_fi(zero_add_above,1) = 0;
                                logsoutData_woi = cat(1,logsoutData_fi,logsoutData_woi);
                            end
                            if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(end) ~= stop_time)
                                zero_add_below = signal_length - m_logsoutData_woi - zero_add_above;
                                logsoutData_fi = [];
                                logsoutData_fi(zero_add_below,1) = 0;
                                logsoutData_woi = cat(1,logsoutData_woi,logsoutData_fi);
                            end
                            if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(1) == start_time) && (logsoutElem(k).Values.Time(2) ~= (start_time+dt))
                                val = logsoutElem(k).Values.Data(1);
                                timevals = start_time:dt:(logsoutElem(k).Values.Time(2)-dt);
                                for i = 1:length(timevals)
                                    datavals = [];
                                    datavals(i) = val;
                                end
                                logsoutData_woi = cat(1,datavals',logsoutData_woi(2:end));
                            end
                            if (m_logsoutData_woi == 1) && (logsoutElem(k).Values.Time(1) == start_time)
                                timevals = start_time+dt:dt:stop_time;
                                for i = 1:length(timevals)
                                    datavals = [];
                                    datavals(i) = 0;
                                end
                                logsoutData_woi = cat(1,datavals',logsoutData_woi);
                            end
                            logsoutData_array(:,index) = double(logsoutData_woi);
                        else
                            logsoutData_array(:,index) = double(logsoutData_woi);
                        end
                        logsoutData_final{index} = logsoutData_woi;
                        index = index + 1;
                    end
                else
                    logsoutData_struct = logsoutElem(k).Values;
                    logsoutData_cell = struct2cell(logsoutData_struct);
                    [m_logsoutData_cell,n_logsoutData_cell] = size(logsoutData_cell);
                    for l = 1:m_logsoutData_cell
                        logsoutData_w = logsoutData_cell{l}.Data;
                        logsoutData_wi = logsoutData_cell{l}.Data;
                        [m_logsoutData_wi,n_logsoutData_wi] = size(logsoutData_wi);
                        if (m_logsoutData_wi < signal_length)
                            if (logsoutData_cell{1}.Time(1) ~= start_time) % Append zeros to the signal in the beginning or end or both as necessary
                                zero_add_above = uint64(logsoutData_cell{1}.Time(1)/dt);
                                logsoutData_f = [];
                                logsoutData_f(zero_add_above,n_logsoutData_wi) = 0;
                                logsoutData_w = cat(1,logsoutData_f,logsoutData_wi);
                            end
                            if (logsoutData_cell{1}.Time(end) ~= stop_time)
                                zero_add_below = signal_length - uint64(logsoutData_cell{1}.Time(end)/dt) - zero_add_above;
                                logsoutData_f = [];
                                logsoutData_f(zero_add_below,n_logsoutData_wi) = 0;
                                logsoutData_w = cat(1,logsoutData_wi,logsoutData_f);
                            end
                        end
                        if (n_logsoutData_wi == 1)
                            logsoutData_array(:,index) = double(logsoutData_w);
                            logsoutData_final{index} = logsoutData_w;
                            index = index + 1;
                        end
                    end
                end
            end
            simout = logsoutData_array;
            SIMOUT_Mutant{pq}{nb_test} = simout;
            rob_mut{pq}{nb_test} = monitorATCS(simout);
        end
    end
    
    ROB_MUT{gg} = rob_mut;
    
    %% ----------------------------------------------------------------------------------
    %% CMA : Classical Mutation Analysis of ATCS
    
    %% Finding the killed mutants
    % Strong mutation testing : check only the outputs
    for i = 1:length(signalNames_o) % for all test cases
        for mutantnum = 1:height(newtable) % for all mutants
            % s42 and s55 are two outputs, respective indices are 1 and 2
            % in simulation output data
            % if any of the outputs is not equal, kill the mutant
            if ~isequal(SIMOUT_Original{1,i}(:,1), SIMOUT_Mutant{1,mutantnum}{1,i}(:,1)) || ~isequal(SIMOUT_Original{1,i}(:,2), SIMOUT_Mutant{1,mutantnum}{1,i}(:,2))
                kill = 1;
            else
                kill = 0;
            end
            kill_info(i,mutantnum) = kill;
        end
    end
    
    Killedmutant_info{gg} = kill_info;
    
    for mutantnum = 1:height(newtable) % for all mutants
        % count the number of tests that kill a mutant
        ntkm(1,mutantnum) = sum(kill_info(:,mutantnum));% number of tests that kill the mutant
        if ntkm(1,mutantnum) > 0
            mutant_killed(1,mutantnum) = 1;% 1 if the mutant is killed by atleast one test
        else
            mutant_killed(1,mutantnum) = 0;
        end
    end
    
    Mutant_killed_analysis{gg} = mutant_killed;
    
    KM = sum(mutant_killed); % Number of killed mutants
    Killablemut = 47; % Number of killable mutants
    LM = Killablemut - KM; % Number of live mutants
    Actual_mutation_score = (KM/Killablemut)*100;
    
    CMA_KM(gg) = KM;
    CMA_LM(gg) = LM;
    CMA_AMS_percentage(gg) = Actual_mutation_score;
    
    
    %% ----------------------------------------------------------------------------------
    %% PMA : Property-based mutation analysis of ATCS
    
    %% Finding the killed mutants
    % Strong mutation testing with RIPR model
    % use the passing/failing verdicts of the test cases w.r.t. phi
    
    %% Make rob value 1 if positive; otherwise 0
    % for original model
    bool_rob_orig = double(cell2mat(rob_o) > 0);
    % for mutants
    for nb_test = 1:length(out_Original)
        for mutantnum = 1:height(newtable)
            bool_rob_mut(nb_test,mutantnum) = double((rob_mut{1,mutantnum}{1,nb_test}) > 0);
        end
    end
    
    %% Find the phi-killed mutants
    for i = 1:length(signalNames_o) % for all test cases
        for mutantnum = 1:height(newtable) % for all mutants
            % phi-kill the mutant if it the test passes on the original model
            % but fails on the mutant
            % i.e., if bool_rob_orig == 1 and bool_rob_mut == 0
            if bool_rob_orig(i) == 1 && bool_rob_mut(i,mutantnum) == 0
                phi_kill = 1;
            else
                phi_kill = 0;
            end
            phi_kill_info(i,mutantnum) = phi_kill;
        end
    end
    
    phiKilledmutant_info{gg} = phi_kill_info;
    
    for mutantnum = 1:height(newtable) % for all mutants
        % count the number of tests that kill a mutant
        ntkm_phi(1,mutantnum) = sum(phi_kill_info(:,mutantnum));% number of tests that kill the mutant
        if ntkm_phi(1,mutantnum) > 0
            mutant_phikilled(1,mutantnum) = 1;% 1 if the mutant is killed by atleast one test
        else
            mutant_phikilled(1,mutantnum) = 0;
        end
    end
    
    Mutant_phikilled_analysis{gg} = mutant_phikilled;
    
    phi_KM = sum(mutant_phikilled); % Number of phi-killed mutants
    phi_killablemut = 47; % Number of phi-killable mutants
    phi_LM = phi_killablemut - phi_KM; % Number of live mutants
    % Mutation score using our approach
    MS_percentage = (phi_KM/phi_killablemut)*100;
    
    PMA_KM(gg) = phi_KM;
    PMA_LM(gg) = phi_LM;
    PMA_MS_percentage(gg) = MS_percentage;
    
    %% %% Actual Mutation score (CMA-based score) and Mutation score MS (PMA-based score) for individual operators
    % We consider Noise, Negate, Bias/Offset, Absolute, S2P, P2S, ASR and LUT
    
    for pq = 1: height(newtable)
        faultoperator_type = cell2mat(table2array(newtable(pq,4))); % Read the fault table to identify the type of the operator
        
        % mark the mutant number that corresponds to the operator type
        if faultoperator_type == "Bias/Offset"
            Offset_fault_mutant(1,pq) = 1;
        else
            Offset_fault_mutant(1,pq) = 0;
        end
        
        if faultoperator_type == "Absolute"
            Absolute_fault_mutant(1,pq) = 1;
        else
            Absolute_fault_mutant(1,pq) = 0;
        end
        
        if faultoperator_type == "Noise"
            Noise_fault_mutant(1,pq) = 1;
        else
            Noise_fault_mutant(1,pq) = 0;
        end
        
        if faultoperator_type == "Negate"
            Negate_fault_mutant(1,pq) = 1;
        else
            Negate_fault_mutant(1,pq) = 0;
        end
        
        if faultoperator_type == "S2P"
            S2P_fault_mutant(1,pq) = 1;
        else
            S2P_fault_mutant(1,pq) = 0;
        end
        
        if faultoperator_type == "P2S"
            P2S_fault_mutant(1,pq) = 1;
        else
            P2S_fault_mutant(1,pq) = 0;
        end
        
        if faultoperator_type == "ASR"
            ASR_fault_mutant(1,pq) = 1;
        else
            ASR_fault_mutant(1,pq) = 0;
        end
        
        if faultoperator_type == "LUT"
            LUT_fault_mutant(1,pq) = 1;
        else
            LUT_fault_mutant(1,pq) = 0;
        end
    end
    
    % Count the number of mutants corresponding to individual operators
    count_negate = sum(Negate_fault_mutant);
    count_absolute = sum(Absolute_fault_mutant);
    count_noise = sum(Noise_fault_mutant);
    count_offset = sum(Offset_fault_mutant);
    count_S2P = sum(S2P_fault_mutant);
    count_P2S = sum(P2S_fault_mutant);
    count_ASR = sum(ASR_fault_mutant);
    count_LUT = sum(LUT_fault_mutant);
    
    % Check if the mutant (for individual operators) is killed by CMA and PMA
    for pq = 1: height(newtable)
        if Offset_fault_mutant(1,pq) == 1
            offset_killed_mutant(1,pq) = mutant_killed(1,pq);
            offset_phikilled_mutant(1,pq) = mutant_phikilled(1,pq);
        else
            offset_killed_mutant(1,pq) = 0;
            offset_phikilled_mutant(1,pq) = 0;
        end
        
        if Noise_fault_mutant(1,pq) == 1
            noise_killed_mutant(1,pq) = mutant_killed(1,pq);
            noise_phikilled_mutant(1,pq) = mutant_phikilled(1,pq);
        else
            noise_killed_mutant(1,pq) = 0;
            noise_phikilled_mutant(1,pq) = 0;
        end
        
        if Absolute_fault_mutant(1,pq) == 1
            absolute_killed_mutant(1,pq) = mutant_killed(1,pq);
            absolute_phikilled_mutant(1,pq) = mutant_phikilled(1,pq);
        else
            absolute_killed_mutant(1,pq) = 0;
            absolute_phikilled_mutant(1,pq) = 0;
        end
        
        if Negate_fault_mutant(1,pq) == 1
            negate_killed_mutant(1,pq) = mutant_killed(1,pq);
            negate_phikilled_mutant(1,pq) = mutant_phikilled(1,pq);
        else
            negate_killed_mutant(1,pq) = 0;
            negate_phikilled_mutant(1,pq) = 0;
        end
        
        if S2P_fault_mutant(1,pq) == 1
            S2P_killed_mutant(1,pq) = mutant_killed(1,pq);
            S2P_phikilled_mutant(1,pq) = mutant_phikilled(1,pq);
        else
            S2P_killed_mutant(1,pq) = 0;
            S2P_phikilled_mutant(1,pq) = 0;
        end
        
        if P2S_fault_mutant(1,pq) == 1
            P2S_killed_mutant(1,pq) = mutant_killed(1,pq);
            P2S_phikilled_mutant(1,pq) = mutant_phikilled(1,pq);
        else
            P2S_killed_mutant(1,pq) = 0;
            P2S_phikilled_mutant(1,pq) = 0;
        end
        
        if ASR_fault_mutant(1,pq) == 1
            ASR_killed_mutant(1,pq) = mutant_killed(1,pq);
            ASR_phikilled_mutant(1,pq) = mutant_phikilled(1,pq);
        else
            ASR_killed_mutant(1,pq) = 0;
            ASR_phikilled_mutant(1,pq) = 0;
        end
        
        if LUT_fault_mutant(1,pq) == 1
            LUT_killed_mutant(1,pq) = mutant_killed(1,pq);
            LUT_phikilled_mutant(1,pq) = mutant_phikilled(1,pq);
        else
            LUT_killed_mutant(1,pq) = 0;
            LUT_phikilled_mutant(1,pq) = 0;
        end
    end
    
    CMA_offset_killed_mutant{gg} = offset_killed_mutant;
    PMA_offset_phikilled_mutant{gg} = offset_phikilled_mutant;
    
    CMA_noise_killed_mutant{gg} = noise_killed_mutant;
    PMA_noise_phikilled_mutant{gg} = noise_phikilled_mutant;
    
    CMA_absolute_killed_mutant{gg} = absolute_killed_mutant;
    PMA_absolute_phikilled_mutant{gg} = absolute_phikilled_mutant;
    
    CMA_negate_killed_mutant{gg} = negate_killed_mutant;
    PMA_negate_phikilled_mutant{gg} = negate_phikilled_mutant;
    
    CMA_S2P_killed_mutant{gg} = S2P_killed_mutant;
    PMA_S2P_phikilled_mutant{gg} = S2P_phikilled_mutant;
    
    CMA_P2S_killed_mutant{gg} = P2S_killed_mutant;
    PMA_P2S_phikilled_mutant{gg} = P2S_phikilled_mutant;
    
    CMA_ASR_killed_mutant{gg} = ASR_killed_mutant;
    PMA_ASR_phikilled_mutant{gg} = ASR_phikilled_mutant;
    
    CMA_LUT_killed_mutant{gg} = LUT_killed_mutant;
    PMA_LUT_phikilled_mutant{gg} = LUT_phikilled_mutant;
    
    % Count the number of mutants killed for individual operators by CMA and
    % PMA and calculate the respective scores
    count_offset_killed = sum(offset_killed_mutant);
    count_offset_phikilled = sum(offset_phikilled_mutant);
    CMA_AMS_Offset = (count_offset_killed/count_offset)*100;
    PMA_MS_Offset = (count_offset_phikilled/count_offset)*100;
    
    count_noise_killed = sum(noise_killed_mutant);
    count_noise_phikilled = sum(noise_phikilled_mutant);
    CMA_AMS_noise = (count_noise_killed/count_noise)*100;
    PMA_MS_noise = (count_noise_phikilled/count_noise)*100;
    
    count_absolute_killed = sum(absolute_killed_mutant);
    count_absolute_phikilled = sum(absolute_phikilled_mutant);
    CMA_AMS_absolute = (count_absolute_killed/count_absolute)*100;
    PMA_MS_absolute = (count_absolute_phikilled/count_absolute)*100;
    
    count_negate_killed = sum(negate_killed_mutant);
    count_negate_phikilled = sum(negate_phikilled_mutant);
    CMA_AMS_negate = (count_negate_killed/count_negate)*100;
    PMA_MS_negate = (count_negate_phikilled/count_negate)*100;
    
    count_S2P_killed = sum(S2P_killed_mutant);
    count_S2P_phikilled = sum(S2P_phikilled_mutant);
    CMA_AMS_S2P = (count_S2P_killed/count_S2P)*100;
    PMA_MS_S2P = (count_S2P_phikilled/count_S2P)*100;
    
    count_P2S_killed = sum(P2S_killed_mutant);
    count_P2S_phikilled = sum(P2S_phikilled_mutant);
    CMA_AMS_P2S = (count_P2S_killed/count_P2S)*100;
    PMA_MS_P2S = (count_P2S_phikilled/count_P2S)*100;
    
    count_ASR_killed = sum(ASR_killed_mutant);
    count_ASR_phikilled = sum(ASR_phikilled_mutant);
    CMA_AMS_ASR = (count_ASR_killed/count_ASR)*100;
    PMA_MS_ASR = (count_ASR_phikilled/count_ASR)*100;
    
    count_LUT_killed = sum(LUT_killed_mutant);
    count_LUT_phikilled = sum(LUT_phikilled_mutant);
    CMA_AMS_LUT = (count_LUT_killed/count_LUT)*100;
    PMA_MS_LUT = (count_LUT_phikilled/count_LUT)*100;
    
    
    
    Num_offset_killed(gg) = count_offset_killed;
    Num_noise_killed(gg) = count_noise_killed;
    Num_absolute_killed(gg) = count_absolute_killed;
    Num_negate_killed(gg) = count_negate_killed;
    Num_S2P_killed(gg) = count_S2P_killed;
    Num_P2S_killed(gg) = count_P2S_killed;
    Num_ASR_killed(gg) = count_ASR_killed;
    Num_LUT_killed(gg) = count_LUT_killed;
    
    
    Num_offset_phikilled(gg) = count_offset_phikilled;
    Num_noise_phikilled(gg) = count_noise_phikilled;
    Num_absolute_phikilled(gg) = count_absolute_phikilled;
    Num_negate_phikilled(gg) = count_negate_phikilled;
    Num_S2P_phikilled(gg) = count_S2P_phikilled;
    Num_P2S_phikilled(gg) = count_P2S_phikilled;
    Num_ASR_phikilled(gg) = count_ASR_phikilled;
    Num_LUT_phikilled(gg) = count_LUT_phikilled;
    
    
    CMA_AMS_Offsetfault(gg) = CMA_AMS_Offset;
    PMA_MS_Offsetfault(gg) = PMA_MS_Offset;
    
    CMA_AMS_noisefault(gg) = CMA_AMS_noise;
    PMA_MS_noisefault(gg) = PMA_MS_noise;
    
    CMA_AMS_absolutefault(gg) = CMA_AMS_absolute;
    PMA_MS_absolutefault(gg) = PMA_MS_absolute;
    
    CMA_AMS_negatefault(gg) = CMA_AMS_negate;
    PMA_MS_negatefault(gg) = PMA_MS_negate;
    
    CMA_AMS_S2Pfault(gg) = CMA_AMS_S2P;
    PMA_MS_S2Pfault(gg) = PMA_MS_S2P;
    
    CMA_AMS_P2Sfault(gg) = CMA_AMS_P2S;
    PMA_MS_P2Sfault(gg) = PMA_MS_P2S;
    
    CMA_AMS_ASRfault(gg) = CMA_AMS_ASR;
    PMA_MS_ASRfault(gg) = PMA_MS_ASR;
    
    CMA_AMS_LUTfault(gg) = CMA_AMS_LUT;
    PMA_MS_LUTfault(gg) = PMA_MS_LUT;
    
end
